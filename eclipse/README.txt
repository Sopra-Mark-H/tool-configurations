================================================================================
== README - Eclipse-Configuration. =============================================
================================================================================

 Contents:
 
 - Project Description.
 - Project Structure.
 - Terms of Use.

================================================================================
== Project Description. ========================================================
================================================================================

 This project contains a collection of useful configuration files for various
 versions of Eclipse, the Java IDE available at www.eclipse.org.

 The configurations available should assist those setting up a fresh
 installation/workspace to achieve a useful working state in the minimum amount
 of time.

 Currently, configurations are available for:

 Common (All Eclipse Releases): Shared English (UK) dictionary file.

 Indigo...: Java Appearance Code Style configuration files.
 Juno.....: Java Appearance Code Style configuration files.
 Kepler...: Java Appearance Code Style configuration files.
 
 Note that the arrangement of configuration files is chosen to help remind
 participants where in the Eclipse 'preferences' menu structure the
 corresponding location is that needs to be edited.
 
 For Eclipse based products like Springsource Tool Set (STS) and JBoss 
 Developer Studio (JBDS) simply identify the underlying version of Eclipse
 that your product is based upon and use the appropriate set of config files.
 
================================================================================
== Project Structure. ==========================================================
================================================================================

 From the top-level directory of this project, the contents are arranged as
 follows:

 Eclipse-Configuration
 |
 |- Common
 |  |
 |  |- General-Editors-Text-Editors-Spelling
 |  |  |
 |  |  |- Dictionary-English-UK.txt
 |
 |- Indigo
 |  |
 |  |- Clean Up - Sopra Default.xml
 |  |- Code Templates - Sopra Default.xml
 |  |- Formatter - Sopra Default.xml
 |  |- Import Order - Sopra Default.importorder
 |
 |- Juno
 |  |
 |  |- Clean Up - Sopra Default.xml
 |  |- Code Templates - Sopra Default.xml
 |  |- Formatter - Sopra Default.xml
 |  |- Import Order - Sopra Default.importorder
 |
 |- Kepler
 |  |
 |  |- Clean Up - Sopra Default.xml
 |  |- Code Templates - Sopra Default.xml
 |  |- Formatter - Sopra Default.xml
 |  |- Import Order - Sopra Default.importorder
 |
 |- Luna
 |  |
 |  |- Clean Up - Sopra Default.xml
 |  |- Code Templates - Sopra Default.xml
 |  |- Formatter - Sopra Default.xml
 |  |- Import Order - Sopra Default.importorder 
 |
 
 Eclipse Menu Locations for Java Code-Style: 
 
 -> Window|Preferences|Java|Code Style
 
 Eclipse Menu Locations for Custom Dictionary:
 
 -> Window|Preferences|General|Editors|Text Editors|Spelling
 
 Simply load any/all required configurations from the 'Common' tree and the tree
 named for the major version of Eclipse being used. Sub-folder names within the
 version trees loosely correspond to the location of the panel in the Eclipse
 preferences dialog where the option to load a given configuration is located.
 
================================================================================
== Terms of Use. ===============================================================
================================================================================

 TBC. This software is copyright and all rights are reserved.

================================================================================
== End of Document. ============================================================
================================================================================
